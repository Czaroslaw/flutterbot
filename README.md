# FlutterBot
This is the last version of my very first public Discord Bot.
While it lasted it was present on over 500 servers. Sadly on April 12th 2019 my account on Discord has been permanently banned without any explanation. Discord's so called "Trust & Safety Team" completely ignored my appeals by sending an automatic message after 4 days. Up to this day Discord is inconsistent with their ban policies ignoring for the most part everything they have accused me for. If you consider getting Discord Nitro, please do not. Spend it on something better, or at least do not get it directly from Discord. Discord as a company has commited a lot of shady practices and they do not deserve your money. Which is a pity because as a platform Discord was good.

## Setting up the bot
Flutterbot uses numerous Node libraries, which you need to install first in order for it to work at all.
Assuming you already have Node installed, open up the terminal or console in the path you chose to clone the bot to.
```
nmp install discord.js colors request jimp mysql
```
The next step is to open `config.js`. This is the file which stores configuration for the bot.
```
module.exports = {
    prefix: "t!", //prefix, can be any string of characters
    derpiKey: "", //Derpibooru API key
    owner: "", //ID of the bot owner
    token: "" //Discord bot token
}
```
Then all it takes is to just run the bot through Node.

**Note:** While you do not need an SQL database for the bot to work, certain commands will not work without it.