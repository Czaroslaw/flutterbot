/*
==FlutterBot==
Created by Czaroslaw
All rights reserved
*/

const Discord = require('discord.js');
const bot = new Discord.Client();
const colors = require('colors');
const teksty = require('./modules/teksty');
const util = require('./modules/util.js');
const time = require('./modules/time.js');
const request = require('request');
const mysql = require('mysql');
const dbconfig = require('./modules/dbconfig.js');
const jimp = require("jimp");
const cfg = require('./config.js');

const owner = cfg.owner;
const token = cfg.token;
const prefix = cfg.prefix;
const derpiKey = cfg.derpiKey;

const bot_version = "1.05";
const start_time = Date.now();

bot.login(token);


function idparse (mention) {
    output = mention.replace("<", "");
    output = output.replace("@", "");
    output = output.replace(">", "");
    return output;
}

bot.on("ready", function () {
    process.stdout.write('\033c'); //clearing the console/terminal
    console.log("Welcome to Flutterbot. Yay~!\n\n".yellow);
    console.log("Copyright 2018 Czaroslaw\n");
    console.log("==READY==".green);

    bot.user.setPresence(
        {
            status: "online",
            game: {
                name: "| f!help",
                url: "http://twitch.tv/dupa"
            }
           
        }
    );

    var connection = mysql.createConnection({
        host: dbconfig.host,
        user: dbconfig.user,
        password: dbconfig.pass,
        database: dbconfig.db
      });
      
      connection.connect(function(err) {
        //if (err) throw err;
        if (err)
        console.log("\nWarning! Couldn't connect to the database!".red)
        else
        console.log("\nConnected to MySQL!".cyan);
      });
      connection.end();
    
});


//cmds

bot.on('message', function (message) {
    

    if (message.content.startsWith(prefix)) {

        var args = message.content.split(prefix);
        var args = args[1].split(" ");

        //command logs

        godzina = message.createdAt + '';
        godzina = godzina.split(" ");

        console.log(godzina[4]+ " " + message.author.username.cyan + ": " + message.content);


        //komendy

        switch (args[0].toLowerCase()) {
            case "ping":
                message.channel.send("Pong! " + bot.pings + "ms");
                break;
            default:
                message.channel.send("Uhm?");
                break;
            case "avatar":
                if (args[1] == undefined && message.author.avatarURL != undefined)
                {
                    message.reply("That's you: " + message.author.avatarURL);
                    break;
                }
                else if (args[1].startsWith("<"))
                {
                    if (message.mentions.users.array().length == 0)
                    {
                        message.channel.send("Uhm... I can't find such a pony...");
                        break;
                    }
                    console.log(args[1]);
                    if (Array.from(message.mentions.users)[0][1].avatarURL != undefined)
                    {
                        message.reply(Array.from(message.mentions.users)[0][1].avatarURL);
                    }
                    else message.channel.send(":warning: This user has no avatar.");
                    
                }
                else message.channel.send(":warning: Not a valid user.");
                break;
            case "roll":
                if (!isNaN(args[1]) && args[1] > 0) message.reply(util.losuj(args[1]) + ":game_die:");
                else message.reply("Uhm... I don't think it can be rolled... Try again...!");
                break;
            case "help":
                var embed = new Discord.RichEmbed()
                .setColor(0xffaeb0)
                .setDescription("**Uhm... Hello~... I'm FlutterBot.** \n\nUseful links: [[invite link]](https://discordapp.com/oauth2/authorize?client_id=377348172810420224&scope=bot&permissions=0) [[support server]](https://discord.gg/utnfrDj)")
                .setTitle("")
                .addField("Available commands:", "\n**fun:**\n`roll <value>` \n`dp <tags>` search for an image from Derpibooru [[search syntax]](https://derpibooru.org/search/syntax)\n`hug <user>` hug somepony!\n`boop <user>` boop somepony!\n`facehoof` random facehoof gif \n`cookie <user>` give someone a cookie :cookie:\n`say` make the bot say something\n`sayd` same as above, but deletes the previous message (requires special permissions)\n`facts` share a fact\n`kiss` kiss somepony!\n\n**utility:**\n`avatar <user>` show mentioned user's avatar \n`info <user>` user info, such as id etc.\n`server` server info\n`bot` bot info\n")
                .setFooter("FlutterBot has been created by Czaroslaw using NodeJS and discord.js library.\nProfile pic made by [suikuzu](https://suikuzu.deviantart.com/)")
                ;
                
                if(message.channel.type != "dm")
                message.channel.send(message.author + " check your DM... I mean... If you don't mind..");

                message.author.send(embed);

                break;

            case "dp":
                var query = '';

                for (i = 1; i <= args.length-1; i++)
                {
                    query = query + args[i] + "+";
                }

                query = query.replace(/&/g, "%26");
               
                if(message.channel.nsfw || message.channel.type == "dm" || message.channel.type == "group")
                {
                   var qURL = "https://derpibooru.org/search.json?q="+ query + "&random_image=1" + "&key=" + derpiKey;
                }
                else
                {
                    var qURL = "https://derpibooru.org/search.json?q="+ query + ",-explicit" + "&random_image=1" + "&key=" + derpiKey;
                }

                var imgid = "";

                request({
                    url: qURL,
                    json: true
                }, function (error, response, iid) {
                    
                    

                    if (!error && response.statusCode == 200 && iid != null && iid.id != undefined) {
                        console.log(iid);
                        imgid = iid.id;

                        request({
                            url: "https://derpibooru.org/"+ imgid + ".json",
                            json: true
                        }, function (error, response, img) {
                        console.log("http:" + img.image);

                        embed = new Discord.RichEmbed()
                            .setColor(0xffaeb0)
                            .setImage("http:" + img.image)
                            .addField("Score: " + img.score , img.tags.substring(0, 1023))
                            .setTitle("#" + img.id)
                            .setURL("https://derpibooru.org/" + img.id);

                            message.channel.send(embed);
                        })

                        }
                    
                    else 
                    {
                        embed = new Discord.RichEmbed()
                            .addField(":warning:", " A problem was encountered. Make sure the tags are correct and if the channel is either SFW or NSFW.");
                            message.channel.send(embed);
                    }
                    
                })
                break;

                case "hug":

                var con = mysql.createConnection({
                    host: dbconfig.host,
                    user: dbconfig.user,
                    password: dbconfig.pass,
                    database: dbconfig.db
                  });
              
                        con.connect(function(err) {
                            if (err) console.log("MySQL Error!".red);
                            con.query("SELECT link from hugs order by rand() limit 1", function (err, result, fields) {
                                var url = '';
                                if (err) 
                                {
                                    console.log("MySQL Error!".red);
                                    //console.log(err);
                                    //message.channel.send(message.author + " gives " + args[1] + " a warm hug!");
                                    url = '';

                                }
                                else

                                    url = result[0].link;
                                    if (message.mentions.users.array().length != 0)
                                    user = Array.from(message.mentions.users)[0][1];
                                    else {
                                        message.channel.send("Uhm... I can't find such a pony...");
                                        return;
                                    }

                                    if (user == bot.user)
                                    message.channel.send(message.author + " **hug**\n" + url);

                                    else if (user == message.author)
                                    message.channel.send(message.author + " hugs himself!");

                                    else  if (user != undefined)
                                { 
                                    if (message.mentions.users.array().length == 0)
                                    {
                                        message.channel.send("Uhm... I can't find such a pony...");
                                        return;
                                    }
                                    else   
                                    console.log(url);
                                    message.channel.send(message.author + " gives " + user + " a warm hug!\n" + url);
                                }
                                else
                                    message.channel.send("Aww... poor you... no one to hug? I can hug you instead! If... If you don't mind...");
                                
                            });
                            con.end();
                          });    
                          
                break;

                //kiss
                case "kiss":
                
    var con = mysql.createConnection({
    host: dbconfig.host,
    user: dbconfig.user,
    password: dbconfig.pass,
    database: dbconfig.db
   });
                              
        con.connect(function(err) {
            if (err) console.log("MySQL Error!".red);
                con.query("SELECT link from kiss order by rand() limit 1", function (err, result, fields) {
            var url = '';
             if (err) 
            {
                console.log("MySQL Error!".red);
                 //console.log(err);
                 //message.channel.send(message.author + " gives " + args[1] + " a warm hug!");
                url = '';
                
            }
            else
                
                                                    url = result[0].link;
                                                
                                                    if (args[1] == bot.user)
                                                    message.channel.send(message.author + " uhm... I'm not sure if I should...");
                
                                                    else if (args[1] == message.author)
                                                    message.channel.send(message.author + " kisses himself!");
                
                                                    else  if (args[1] != undefined)
                                                { 
                                                    if (message.mentions.users.array().length == 0)
                                                    {
                                                        message.channel.send("Uhm... I can't find such a pony...");
                                                        return;
                                                    }
                                                    else   
                                                    console.log(url);
                                                    message.channel.send(message.author + " gives " + args[1] + " a smooch!\n" + url);
                                                }
                                                else
                                                    message.channel.send("Silly pony, you can't kiss no pony.");
                                                
                                            });
                                            con.end();
    });    
    
break;



                //boop
 case "boop":

                    var con = mysql.createConnection({
                        host: dbconfig.host,
                        user: dbconfig.user,
                        password: dbconfig.pass,
                        database: dbconfig.db
                    });
              
                        con.connect(function(err) {
                            if (err) console.log("MySQL Error!".red);
                            con.query("SELECT url from boop order by rand() limit 1", function (err, result, fields) {
                                var url = '';
                                if (err) 
                                {
                                    console.log("MySQL Error!".red);
                                    url = '';

                                }
                                else
                                    //console.log(result);
                                    if(result != undefined)
                                    url = result[0].url;

                                    if (message.mentions.users.array().length != 0)
                                    user = Array.from(message.mentions.users)[0][1];
                                
                                    if (user == bot.user)
                                    message.channel.send(message.author + " **boop**\n" + url);

                                    else if (user == message.author)
                                    message.channel.send(message.author + " boops himself!");

                                    else if (user != undefined)
                                {    
                                    if (message.mentions.users.array().length == 0)
                                    {
                                        message.channel.send("Uhm... I can't find such a pony...");
                                        return;
                                    }
                                    else
                                    console.log(url);
                                    message.channel.send(message.author + " boops " + user + "!\n" + url);
                                }
                                else
                                    message.channel.send("B-boop?");
                                
                            });
                            con.end();
                          });    
                          
                break;                



                case "cookie":
                    if (message.mentions.users.array().length != 0)
                    user = Array.from(message.mentions.users)[0][1];
                    else
                    {
                    message.channel.send("You can't give a cookie to no pony.");
                    break;
                    }

                    if (user == bot.user)
                    message.channel.send(message.author + " is... Is that really for me? I... I don't know what to say... Thank you...~ ");
                    else if (user == message.author)
                    message.channel.send(message.author + " gives himself a cookie! :cookie:");
                    else  if (args[1] != undefined)
                    {
                        if (message.mentions.users.array().length == 0)
                        {
                            message.channel.send("Uhm... I can't find such a pony...");
                            break;
                        }
                        else
                    message.channel.send(message.author + " gives " + user + " a cookie! :cookie:");
                    }
                    
                
                break;
                case "greentext":
                break;
                case "info": 

                    var info = message.author;

                    embed = new Discord.RichEmbed()
                    .setTitle(info.username + "#" + info.discriminator)
                    .addField("ID", "`" + info.id + "`", true)
                    .addField("Mention", "<@" + info.id + ">", true) 
                    .addField("Is a bot?", info.bot, true)                   
                    .setColor(0xffaeb0)
                    .setThumbnail(info.avatarURL)
                    ;
                    
                    

                    if (args[1] == undefined)
                    {
                       //info = message.author;
                       message.channel.send(embed);
                    }
                    else if (args[1].startsWith("<"))
                    {
                        if (message.mentions.users.array().length == 0)
                        {
                            message.channel.send("Uhm... I can't find such a pony...");
                            break;
                        }
                        
                        info = Array.from(message.mentions.users)[0][1];

                        embed = new Discord.RichEmbed()
                        .setTitle(info.username + "#" + info.discriminator)
                        .addField("ID", "`" + info.id + "`", true)
                        .addField("Mention", "<@" + info.id + ">", true) 
                        .addField("Is a bot?", info.bot, true)                   
                        .setColor(0xffaeb0)
                        .setThumbnail(info.avatarURL)
                        ;

                        message.channel.send(embed);
                        //console.log(info);

                    }
                    else message.channel.send(":warning: Not a valid user.");

                   
                break;

                case "facehoof":
                var con = mysql.createConnection({
                    host: dbconfig.host,
                    user: dbconfig.user,
                    password: dbconfig.pass,
                    database: dbconfig.db
                  });
              
                        con.connect(function(err) {
                            if (err) console.log("MySQL Error!".red);
                            con.query("SELECT link from facehoof order by rand() limit 1", function (err, result, fields) {
                                if (err) 
                                {
                                    console.log("MySQL Error!".red);
                                    message.channel.send(":warning: Database error.")

                                }
                                else
                                {
                                    embed = new Discord.RichEmbed()
                                    .setColor(0xffaeb0)
                                    .setImage(result[0].link);
                                    message.channel.send(embed);
                                }
                                    
                        
                            });
                            con.end();
                          });    
                break;

                case "say":
                var text = '';
                    for (i = 1; i <= args.length-1; i++)
                    {
                     text = text + args[i] + " ";
                    }
                message.channel.send(text);
                break;
                case "sayd":
                var text = '';
                for (i = 1; i <= args.length-1; i++)
                {
                 text = text + args[i] + " ";
                }
                message.channel.send(text);
                message.delete();
                break;

                case "serv":

                    if(message.author.id != owner) break;

                    serwery = Array.from(bot.guilds);
                
                    msg = "Total servers: " + serwery.length + "\n";

                    if(serwery.length > 40)
                    {                       

                        powtorzenia = Math.ceil(serwery.length/40);
                        var x = 1;
                        for (j = 1; j <= powtorzenia; j++)
                        {
                            for(i=x-1; i<=x+39; i++)
                            {
                                if(serwery[i] == undefined) break;
                                serw = serwery[i][1];
                                msg = msg + serw.name + ", " + serw.memberCount + ", " + "<@" + serw.ownerID + ">" + "\n";
                            }
                            message.channel.send(msg);
                            x = x + 41;
                            msg = "";
                        }
                    }
                    
                    {
                        for(i=0; i<=serwery.length-1; i++)
                        {
                            serw = serwery[i][1];
                            msg = msg + serw.name + ", " + serw.memberCount + ", " + "<@" + serw.ownerID + ">" + "\n";
                        }
                        message.channel.send(msg);
                    }
                break;

                case "server":

                if (message.channel.type != "text") {
                    message.channel.send(":warning: Not in a server!");
                    break
                };

                var server = message.guild;

                    embed = new Discord.RichEmbed()
                    .setTitle(server.name)
                    //.setDescription("**ID: **`" + server.id + "`\n**Created at:** `" + server.createdAt + "`\n**Owner:** " + server.owner + '\n**Region**: `' + server.region + "`\n**Members:** `" + server.memberCount + "`\n**Icon URL:** `" + server.iconURL + '`\n')               
                    .setColor(0xffaeb0)
                    .setThumbnail(server.iconURL)
                    .addField("ID", "`" + server.id + "`")
                    .addField("Created at", "`"+ server.createdAt + "`")
                    .addField("Owner", server.owner)
                    .addField("Region", "`" + server.region + "`", true)
                    .addField("Member count", "`" + server.memberCount + "`", true)
                    .addField("Icon URL", "`" + server.iconURL + "`")

                    ;

                //message.channel.send('**' + s_name + '**\n **ID:**' );
                message.channel.send(embed);
                //console.log(server.roles);

                break;


                case "facts":

                    var text = '';
                    for (i = 1; i <= args.length-1; i++)
                    {
                    text = text + args[i] + " ";
                    }

                    if (text.length > 120) 
                    {
                        message.channel.send(":warning: The text is too long (max 120 characters)");
                        break;
                    }

                    else if (text.length == 0)
                    {
                        message.channel.send(":warning: The text is too short.");
                        break;
                    }


                    
                    
                    jimp.read("facts.png", function (err, image) {
                        if (err) 
                        {
                            message.channel.send(":warning: Error!");
                            console.log(err);
                            return;
                        }

                        jimp.loadFont(jimp.FONT_SANS_32_BLACK).then(function (font) {
                             if (text.length <= 24)
                             {
                                image.print(font, 50,700, text)
                                image.quality(40)
                                .write("output.jpg");

                             }
                             else
                             {
                                x = 30;
                                y = 620;
                                rep = 1;
                                text = text.split(" ");
                                for (j = 1; j <= 6; j++)
                                {
                                    text2 = "";                   
                                    for (i = rep; i <= rep + 2; i++)
                                    {
                                        if (text[i] == undefined) break;
                                        text2 = text2 + " " + text[i-1];

                                    }
                                    image.print(font, x,y, text2)
                                    y = y + 30;
                                    rep = rep + 3;

                                }
                                image.quality(40)
                                .write("output.jpg");
                                 
                             }

                             message.channel.send("", {
                                files: ["./output.jpg"]
                              })
                        });
                        

                    });

                break;

            case "presence": //resetting the presence if needed
                if(message.author.id == cfg.owner)
                {
                    splitter = cfg.prefix + "presence"; //string that splits the text allowing to add additional text
                    statustext = message.content.split(splitter)[1]; //additional text in the status
                    bot.user.setPresence(
                        {
                            status: "online",
                            game: {
                                name: "| f!help" + statustext,
                                url: "http://twitch.tv/dupa"
                            }
                           
                        }
                    );
                    message.channel.send("Successfully reset the presence!");
                }
                else
                {
                    message.channel.send(":warning: You're not allowed to do this!");
                }
            break

            case "seenon": //servers the user has been seen on
                if (message.mentions.users.array().length != 0)
                {
                    user = Array.from(message.mentions.users)[0][1];
                    console.log();
                }
            break;
    
        case "bot": //bot info

            embed = new Discord.RichEmbed()
            .setTitle("FlutterBot")
            .addField("Uptime", "`" + time.difference(Date.now(), start_time) + "`")
            .addField("Version", "`" + bot_version + "`", true) 
            .addField("Servers", "`" + Array.from(bot.guilds).length + "`", true)                 
            .setColor(0xffaeb0)
            .setFooter("Created by Czaroslaw")
            //.setThumbnail(bot.Client.avatarURL)
            ;
        message.channel.send(embed);

        break;

        }
        return;
    }

    //replying

    if (message.author.equals(bot.user)) return; //ignoring own messages

    for (i = 0; i <= triggerWord.length; i++) {
        if (message.content.toLowerCase().includes(triggerWord[i])) {
            message.channel.send(response[i]);
            return;
        }
    }

 

});
