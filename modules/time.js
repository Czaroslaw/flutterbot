module.exports = {
    difference: function (current, previous) {
        var ms;
        var seconds;
        var minutes;
        var hours;
        var days;
        var months;
        var years;

        var text = "";

        var elapsed = current - previous;

        var seconds = Math.floor(elapsed / 1000);
        var minutes = Math.floor(seconds / 60);
        var hours = Math.floor(minutes / 60);
        var days = Math.floor(hours / 24);
        var months = Math.floor(days / 31);
        var years = Math.floor(months / 12);
        var ms = elapsed - seconds * 1000;

       /* var seconds = Math.floor(elapsed / 1000) - minutes * 60; //avoiding more than 60 seconds
        var minutes = Math.floor(seconds / 60) - hours * 60;
        var hours = Math.floor(minutes / 60) - days * 24;
        var days = Math.floor(hours / 24) - months * 31;
        var months = Math.floor(days / 31) - years * 12;*/


        if (years > 0)
        {
            text +=  years + " years ";
        }
        var months = Math.floor(days / 31) - years * 12;
        if (months > 0)
        {
            text +=  months + " months ";
        }
        var days = Math.floor(hours / 24) - months * 31;
        if (days > 0)
        {
            text +=  days + " days ";
        }
        var hours = Math.floor(minutes / 60) - days * 24;
        if (hours > 0)
        {
            text += hours + " hours ";
        }
        var minutes = Math.floor(seconds / 60) - hours * 60;
        if (minutes > 0)
        {
            text += minutes + " minutes ";
        }
        var seconds = Math.floor(elapsed / 1000) - minutes * 60; //avoiding more than 60 seconds
        if (seconds > 0)
        {
            text += seconds + " seconds ";
        }
        if (ms > 0)
        {
            text += ms + " miliseconds";
        }
        
        return text;
   }
}